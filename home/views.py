from django.shortcuts import render_to_response
from django.template.context import RequestContext
from registration.models import ReferCodes
from registration.forms import ReferForm
from registration.utils import gen_code
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from jobs.models import Job
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"


@login_required
def dashboard(request):
    try:
        my_jobs = Job.jobs.filter(owner=request.user)
    except TypeError:
        pass
    form = ReferForm()
    if request.POST:

        code = gen_code()
        obj, new = ReferCodes.objects.get_or_create(code=code, user=request.user)

        if new:
            site = get_current_site(request)
            url = 'http://' + site.name + reverse('registration_register') + code + '/'
            message = "Hello, \n Please register for the writing service here {0}".format(url)
            send_mail('Subject here', message, settings.EMAIL_HOST_USER,
                      [request.POST['email']], fail_silently=False)

    return render_to_response("dashboard.html", locals(), context_instance=RequestContext(request))


