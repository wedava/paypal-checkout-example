from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from .models import Job
from .forms import JobForm
from filetransfers.api import serve_file
from django.shortcuts import get_object_or_404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin



def download_handler(request, pk):
    job = get_object_or_404(Job, pk=pk)
    return serve_file(request, job.attachment)


class JobCreateView(SuccessMessageMixin, CreateView):
    model = Job
    form_class = JobForm
    template_name = "jobs/create_job.html"
    success_message = "Job created successfully"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(JobCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        job = form.save(commit=False)
        job.owner = self.request.user
        job.status = "Processing"
        return super(JobCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('paypal_pay', args=[str(self.object.id)])


class JobUpdateView(UpdateView):
    model = Job
    form_class = JobForm
    template_name = "jobs/edit_job.html"
    success_message = "Job updated successfully"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        # Job cannot be updated if it is closed or pending.
        status = self.get_object().status
        if status in ["Closed", "Pending"]:
            messages.add_message(self.request, messages.ERROR, "Only open jobs can be updated")
            return HttpResponseRedirect(reverse('dashboard'))
        return super(JobUpdateView, self).dispatch(*args, **kwargs)


class JobDetailView(DetailView):
    model = Job
    template_name = "jobs/job_details.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(JobDetailView, self).dispatch(*args, **kwargs)


class JobDeleteView(DeleteView):
    model = Job
    template_name = "jobs/delete_job.html"

    def get_success_url(self):
        return reverse('dashboard')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        # Job cannot be deleted if it is pending.
        status = self.get_object().status
        if status in ["Pending"]:
            messages.add_message(self.request, messages.ERROR, "Pending jobs cannot be closed")
            return HttpResponseRedirect(reverse('dashboard'))
        return super(JobDeleteView, self).dispatch(*args, **kwargs)
