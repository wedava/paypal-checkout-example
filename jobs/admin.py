from django.contrib import admin
from .models import Job, Solution

class JobModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'status']


admin.site.register(Job, JobModelAdmin)
admin.site.register(Solution)
