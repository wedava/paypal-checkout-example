from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^create/$', JobCreateView.as_view(), name='create_job'),
    url(r'^details/(?P<pk>[^/]+)/$', JobDetailView.as_view(), name='job_details'),
    url(r'^update/(?P<pk>[^/]+)/$', JobUpdateView.as_view(), name='update_job'),
    url(r'^delete/(?P<pk>[^/]+)/$', JobDeleteView.as_view(), name='delete_job'),
    url(r'^download/(?P<pk>[^/]+)/$', download_handler, name='download'),
]
