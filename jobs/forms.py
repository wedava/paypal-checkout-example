from .models import Job
from django import forms


class JobForm(forms.ModelForm):
    deadline = forms.DateField(input_formats=['%Y-%m-%d', '%m/%d/%Y', '%m/%d/%y'], required=False,
                               widget=forms.DateInput(format='%Y-%m-%d'))

    class Meta:
        model = Job
        exclude = ['id', 'owner', 'status', 'date_posted']
