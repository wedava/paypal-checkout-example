from django.db import models
from django.contrib.auth.models import User
import uuid
import os
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime


STATUS_CHOICES = (
    ('Open', 'Open'),
    ('Pending', 'Pending'),
    ('Closed', 'Closed'),
    ('Re-opened', 'Re-opened'),
    ('Processing', 'Processing')
)

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('attachments', filename)

def get_solution_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('solutions', filename)


class Job(models.Model):
    """Jobs"""
    jobs = models.Manager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=250)
    description = models.TextField()
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Open')
    price_per_page = models.FloatField()
    number_of_pages = models.IntegerField()
    date_posted = models.DateField(auto_now_add=True)
    deadline = models.DateField()
    attachment = models.FileField(upload_to=get_file_path, blank=True, null=True)

    def __str__(self):
        return self.title

    def clean(self):
        if datetime.datetime.now().date() > self.deadline:
            raise ValidationError(_('Deadline cannot be a date in the past'))


class Solution(models.Model):
    """Solutions to the posted jobs"""
    solutions = models.Manager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job = models.OneToOneField(Job)
    user = models.OneToOneField(User)
    attachment = models.FileField(upload_to=get_solution_path)

    def __str__(self):
        return self.job.title


