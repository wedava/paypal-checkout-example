from django.db import models
from django.contrib.auth.models import User
import uuid
import os


def get_file_path(filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('avatars', filename)

TITLE_CHOICES = (
    ('Mr', 'Mr'),
    ('Ms', 'Ms'),
    ('Mrs', 'Mrs'),
)


class UserProfile(models.Model):
    """User profiles"""
    profile = models.Manager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User)
    first_name = models.CharField(max_length=40, blank=True, null=True)
    last_name = models.CharField(max_length=40, blank=True, null=True)
    avatar = models.ImageField(upload_to=get_file_path, null=True, blank=True)
    title = models.CharField(max_length=3, choices=TITLE_CHOICES, blank=True, null=True)
    role = models.CharField

    def __str__(self):
        return self.user.username
