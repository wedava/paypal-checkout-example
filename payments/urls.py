from django.conf.urls import url, include
from .views import *

urlpatterns = [
    url(r'^paypal_save/', include('paypal.standard.ipn.urls')),
    url(r'^notify/paypal/$', paypal_ipn, name='ipn'),
    url(r'^paypal/(?P<job_id>[^/]+)/$', paypal_payments_view, name='paypal_pay'),
    url(r'^paypal/refund/(?P<job_id>[^/]+)/$', paypal_refund, name='paypal_refund'),
]
