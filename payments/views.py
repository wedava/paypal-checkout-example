from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render_to_response, HttpResponseRedirect, Http404
from jobs.models import Job
from functools import reduce
from django.contrib.auth.decorators import login_required
import uuid
from django.conf import settings
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
import urllib.parse
from django.contrib import messages
from paypal.standard.ipn.models import PayPalIPN
import requests
from urllib.parse import unquote, parse_qs


def generate_invoice_id():
    invoice_no = str(uuid.uuid4())[24:]
    try:
        PayPalIPN.objects.get(invoice=invoice_no)
        generate_invoice_id()
    except PayPalIPN.DoesNotExist:
        return invoice_no


@login_required
def paypal_payments_view(request, job_id):
    obj = get_object_or_404(Job, id=job_id)
    try:
        PayPalIPN.objects.get(item_number=job_id)
        messages.add_message(request, messages.INFO, "You have already paid for this job.")
        return HttpResponseRedirect(reverse('dashboard'))
    except PayPalIPN.DoesNotExist:
        pass
    title_price_and_pages = (obj.title, obj.price_per_page, obj.number_of_pages)
    total_price = reduce(lambda x, y: x * y, title_price_and_pages[1:])
    item_name = "Payment for job '{0}'".format(title_price_and_pages[0])
    invoice_id = generate_invoice_id()
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "amount": total_price,
        "item_name": item_name,
        "invoice": invoice_id,
        "item_number": job_id,
        "notify_url": settings.SITE_URL + reverse('ipn'),
        "return_url": settings.SITE_URL + "paypal_return/",
        "cancel_return": settings.SITE_URL + "paypal_cancel/",

    }
    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render_to_response("payments/paypal.html", locals(), context_instance=RequestContext(request))

@csrf_exempt
def paypal_ipn(request):
    body = urllib.parse.parse_qs(request.body)
    item_no = body[b'item_number'][0].decode('ascii')
    job = Job.jobs.get(id=item_no)
    if body[b'payment_status'] == [b'Refunded']:
        job.status = "Closed"
        job.save()
    if body[b'payment_status'] == [b'Completed']:
        job.status = "Open"
        job.save()
    return HttpResponseRedirect(reverse('paypal-ipn'))

@login_required
def paypal_refund(request, job_id):
    try:
        job = Job.jobs.get(id=job_id)
    except job.DoesNotExist:
        return Http404("No such job exists")
    try:
        refund_job = PayPalIPN.objects.get(item_number=job_id, payment_status="Completed")
    except refund_job.DoesNotExist:
        return Http404("No such job was paid for")
    try:
        PayPalIPN.objects.get(item_number=job_id, payment_status="Refunded")
        job.status = "Closed"
        job.save()
        msg = "Payment for job '{0}' was already refunded".format(job.title)
        messages.add_message(request, messages.INFO, msg)
    except PayPalIPN.DoesNotExist:
        pass
    transaction_endpoint = "https://api-3t.sandbox.paypal.com/nvp"
    payload = {
        'user': settings.PAYPAL_WPP_USER,
        'pwd': settings.PAYPAL_WPP_PASSWORD,
        'signature': settings.PAYPAL_WPP_SIGNATURE,
        'method': 'RefundTransaction',
        'transactionid': refund_job.txn_id,
        'refundtype': 'Full',
        'refundsource': 'any',
        'version': '94'
    }
    transactions = requests.post(transaction_endpoint, data=payload).text
    t1 = unquote(transactions)
    t2 = parse_qs(t1)
    import sys
    sys.stderr.write(str(t2))
    if t2['ACK'][0] == "Failure":
        error_msg = t2["L_LONGMESSAGE0"][0] + ". " + "Amount is not refundable"
        messages.add_message(request, messages.ERROR, error_msg)
    else:
        messages.add_message(request, messages.SUCCESS, "Request for refund successful")
    return HttpResponseRedirect(reverse('job_details', args=[job_id]))
