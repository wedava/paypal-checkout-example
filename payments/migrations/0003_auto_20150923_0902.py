# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0002_payrecord_fee'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payrecord',
            name='job',
        ),
        migrations.RemoveField(
            model_name='payrecord',
            name='user',
        ),
        migrations.DeleteModel(
            name='PayRecord',
        ),
    ]
